program grower;
{$H+}{$mode objfpc}
//This file is free software, it can be distributed under the terms of GNU GPL 2 or later
//This file comes with absolutely no warranty.
//Copyright (C) 2015 by Vasily I. Volchenko (daesher@mail.ru)
uses SysUtils,math;
const BufSize=4096;
var f1,f2:file;
    fn1,fn2,s:string;
    s1,s2,sz:int64;
    buf:array[1..4097] of byte;
    i:int64;
    ok1,ok2,ok3,ok4,ok5:boolean;
    Mul: Integer;
    cnt: Integer;
begin
  ok1:=false;ok2:=false;ok3:=false;
  for i:=1 to ParamCount do
  begin
    s:=ParamStr(i);
    if LeftStr(s,4)='-if=' then
     begin fn1:=RightStr(s,Length(s)-4);ok1:=true;end;
    if LeftStr(s,4)='-of=' then
     begin fn2:=RightStr(s,Length(s)-4);ok2:=true;end;
    if LeftStr(s,4)='-os=' then
     begin s:=Trim(RightStr(s,Length(s)-4));
           if LowerCase(s[length(s)])='k' then begin Mul:=1024;s:=LeftStr(S,Length(s)-1);end;
           if LowerCase(s[length(s)])='m' then begin Mul:=1024*1024;s:=LeftStr(S,Length(s)-1);end;
           if LowerCase(s[length(s)])='g' then begin Mul:=1024*1024*1024;s:=LeftStr(S,Length(s)-1);end;
           s2:=StrToIntDef(s,-1)*Mul;
           ok3:=true;
     end;
  end;
  ok4:=s2>0;ok5:=FileExists(fn1);
  if ok1 and ok2 and ok3 and ok4 and ok5 then
   Writeln('All parameters seems OK, proceeding') else
  begin
    Write('Error in ');
    if not ok1 then write('input filename - absence ');
    if not ok2 then write('output filename - absence ');
    if not ok3 then write('output file size - absence ');
    if not ok4 then write('output file size - invalid ');
    if not ok5 then write('input file name - not exists ');
    writeln;writeln('Usage:'+ExtractFileName(ParamStr(0))+'-if=<input fname> '+
    '-of=<output fname> -os=<output size>');
    halt;
  end;
  AssignFile(f1,fn1);
  AssignFile(f2,fn2);
  Reset(f1,1);Rewrite(f2,1);
  s1:=FileSize(f1);
  i:=0;cnt:=0;
  Write('Progress copying:');
  repeat
    BlockRead(f1,buf,Min(BufSize,s1-i),sz);
   // write('r');
    BlockWrite(f2,Buf,Min(sz,s2-i),sz);
   // write('w');
    i:=i+sz;
    inc(cnt);if cnt=1024 then begin write(i div (1024*1024),' M...');cnt:=0;end;
  until (i>=s1)or(i>=s2);
  writeln;
  FillByte(Buf,BufSize,0);
  if i<s2 then Write('Growing:');
  while i<s2 do
  begin
    BlockWrite(f2,Buf,Min(BufSize,s2-i),sz);
    i:=i+sz;
    inc(cnt);if cnt=1024 then begin write(i div (1024*1024),' M...');cnt:=0;end;
  end;
  CloseFile(f1);CloseFile(F2);
  Writeln;Writeln('All OK');
end.